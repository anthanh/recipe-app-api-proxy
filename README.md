# Recipe API proxy

NGINX proxy app for our recipe app API

## Usage

### Environment Variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the appto forward request to (defaults: `app`)
* `APP_PORT` - Port of the app to forward request to (default: `9000`)
* 